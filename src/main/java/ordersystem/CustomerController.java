package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    
    /**
     * Base
     * Design a REST API request to get an array of customer objects, 
     * that includes all customers in the database.
     */
    @GetMapping("/customers/allCustomers")
    public ResponseEntity<Object> getAllCustomer( ) {
    	if (customerDb.isEmpty() == true) {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	} else {
    		return new ResponseEntity<>(customerDb.values(), HttpStatus.OK);
    	}
    } 
    
    /*
     * Base part2
     */
    @PutMapping("/customers/upDate/{number}")
    public ResponseEntity<Object> getCustomerByNum(@PathVariable long number, @RequestBody Customer customer) {
    	 
    	
    	if (customerDb.containsKey(number)== true) {
    		
    		customerDb.get(number).setFirstName(customer.getFirstName());
    		customerDb.get(number).setLastName(customer.getLastName());

            return new ResponseEntity<>("Updated", HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("No customer exist with that Number", HttpStatus.NOT_FOUND);
    	}
    } 
    
    
    /**
     * Intermediate 1
     * Design and implement a REST API request to get a customer’s
     *  number given their first and last names.
     * @param firstName
     * @param lastName
     * @return
     */
    @GetMapping("/customer/{firstName}/{lastName}")
    public ResponseEntity<Object> getCustomersOrderNumbers(@PathVariable String firstName, @PathVariable String lastName) {
    	List<Long> customeridlist = new ArrayList<Long>();
    		customerDb.forEach( (k,v) -> {
    			if (v.getFirstName().equals(firstName) & v.getLastName().equals(lastName)) {
    	customeridlist.add(v.getNumber());
    			}
    		});

    			if (customeridlist.size()!= 0) {
            return new ResponseEntity<>(customeridlist, HttpStatus.OK);
    			} else {
    				return new ResponseEntity<>(" No Customers with the given first and last name", HttpStatus.NOT_FOUND);
    			}
    } 
    	
    /**
     * Intermediate 2
     * Design and implement a REST API request to get all of a customer’s order numbers given 
     * their customer number.
     * @param number
     * @return
     */
    
    @GetMapping("/orders/{customernumber}")
    public ResponseEntity<Object> getCustomersOrderNumbers(@PathVariable long number) {
    		List<Long> orderID = new ArrayList<Long>();
    		orderDb.forEach( (k,v) -> {
    			if (v.getCustomerNumber() == number) {
    				orderID.add(v.getCustomerNumber());
    			}
    		});

    			if (orderid.size()!= 0) {
    				return new ResponseEntity<>(orderID, HttpStatus.OK);
    			} else {
    				return new ResponseEntity<>("The Customer had no Orders", HttpStatus.NOT_FOUND);
    			}
    		} 

    /**
     * Advanced
     *  Q1
     */
    @GetMapping("/orders/getAll")
    public ResponseEntity<Object> gettingCustomersAllOrders()
    		if(orderDb.size() == 0) {
    			return new ResponseEntity<>("No order exists in Db",  HttpStatus.NOT_FOUND);
    		}else {
    			return new ResponseEntity<>(orderDb.values(),  HttpStatus.OK);

    		}
}